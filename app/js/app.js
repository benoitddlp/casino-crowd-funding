angular.module('CasinoCrowdFunding', [
    'CasinoCrowdFunding.services',
    'CasinoCrowdFunding.controllers',
    'ngRoute',
    'LocalForageModule',
    'n3-pie-chart',
    'CasinoCrowdFunding.filters',
    'ui-notification',
])
    .run([
        '$localForage',
        '$rootScope',
        function ($localForage, $rootScope)
        {
            $rootScope.getUser = function ()
            {
                return $localForage.getItem("user").then(function (user)
                {
                    $rootScope.user = user;
                    return user;
                });
            };

            $rootScope.setUser = function (user)
            {
                $rootScope.user = user;
                return $localForage.setItem("user", user);
            };

            $rootScope.logOut = function ()
            {
                delete $rootScope.user;
                return $localForage.removeItem("user");
            };
            $rootScope.getUser();
        }])
    .run([
        'embeddedDbDAOService',
        '$localForage',
        function (embeddedDbDAOService, $localForage)
        {
            var initted = $localForage.getItem("initted").then(function (initted)
            {
                if (!initted)
                {

                    embeddedDbDAOService.update("projects", {
                        address: "Nantes",
                        addressurl: "https://maps.googleapis.com/maps/api/staticmap?center=Nantes,France&zoom=13&size=255x173&maptype=roadmap&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318&markers=color:red%7Clabel:C%7C40.718217,-73.998284",
                        age: 32,
                        amount: 155000,
                        deadline: "2015-12-30",
                        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus ultricies nulla id facilisis. Nunc iaculis, elit eget volutpat consequat, elit ipsum porttitor augue, vitae interdum eros elit quis orci. Mauris sagittis, ipsum at fringilla dignissim, metus mauris fermentum nisl, id commodo ligula mauris vel ex. Sed quis neque sit amet nisi consequat viverra. Aliquam erat volutpat. Morbi blandit bibendum lorem, ullamcorper elementum libero malesuada et. Nullam mattis nunc vel nulla sollicitudin luctus. Fusce vel pharetra enim, et sagittis augue. Proin nec vehicula augue. Donec sagittis suscipit sapien ac cursus. Ut placerat tellus vel eros elementum condimentum. Vestibulum nec ligula nisi.",
                        firstname: "Pierre",
                        id: 1430403078602,
                        investment: 35000,
                        investmentNb: 7,
                        surname: "Durand",
                        title: "Candidature franchise NANTES Z.A. [test]"
                    }).then(function ()
                    {

                        embeddedDbDAOService.update("projects", {
                            address: "Saint-Etienne",
                            addressurl: "https://maps.googleapis.com/maps/api/staticmap?center=Saint-Etienne,France&zoom=13&size=255x173&maptype=roadmap&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318&markers=color:red%7Clabel:C%7C40.718217,-73.998284",
                            age: 44,
                            amount: 55000,
                            deadline: "2016-07-19",
                            description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce vel diam at est rhoncus auctor vitae vitae nunc. Aenean volutpat velit sapien, ac dignissim elit porttitor a. Vivamus fermentum efficitur dolor, nec laoreet orci fermentum eget. Aliquam sem urna, placerat in ex ut, porttitor ultricies sem. Nulla tincidunt finibus efficitur. Mauris luctus turpis sed finibus fringilla. Vivamus tincidunt nisi a tellus viverra, eu hendrerit felis semper. Cras consequat, nibh porttitor euismod hendrerit, erat quam fringilla sapien, nec euismod eros ligula sed massa. Donec ut iaculis sapien. Phasellus accumsan augue et imperdiet rutrum. Aenean vel aliquam justo, tincidunt tincidunt orci. Proin ultrices est a pulvinar euismod.",
                            firstname: "Laurent",
                            id: 1430403238477,
                            investment: 45000,
                            investmentNb: 9,
                            surname: "Dupont",
                            title: "Un nouveau SPAR à Saint-Etienne"
                        }).then(function ()
                        {

                            embeddedDbDAOService.update("projects", {
                                address: "Lyon Croix-Rousse",
                                addressurl: "https://maps.googleapis.com/maps/api/staticmap?center=Lyon+Croix-Rousse,France&zoom=13&size=255x173&maptype=roadmap&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318&markers=color:red%7Clabel:C%7C40.718217,-73.998284",
                                age: 36,
                                amount: 75000,
                                deadline: "2018-03-25",
                                description: "Nam sodales nibh ac ultricies commodo. Morbi odio elit, interdum sit amet luctus sit amet, consectetur feugiat nunc. Vivamus feugiat aliquet tortor, quis ullamcorper lacus mollis in. Sed at mauris at nunc tempus euismod. Donec suscipit in mauris a molestie. In porta sem eget egestas feugiat. Pellentesque vulputate, orci id faucibus faucibus, mauris dolor sodales velit, et tincidunt metus nisl non dui. Integer vel erat nec arcu aliquet euismod.",
                                firstname: "Benoît",
                                id: 1430403292059,
                                investment: 10000,
                                investmentNb: 2,
                                surname: "D.",
                                title: "Franchise SPAR LYON"
                            }).then(function ()
                            {

                                embeddedDbDAOService.update("projects", {
                                    address: "Brest",
                                    addressurl: "https://maps.googleapis.com/maps/api/staticmap?center=Brest,France&zoom=13&size=255x173&maptype=roadmap&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318&markers=color:red%7Clabel:C%7C40.718217,-73.998284",
                                    age: 33,
                                    amount: 55000,
                                    deadline: "2016-01-28",
                                    description: "Proin nec massa volutpat, auctor nunc id, venenatis nunc. Integer sed orci sed nulla ultricies rutrum. Vivamus libero sem, consequat eget varius ac, tempus vitae orci. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer tempus et risus a ultricies. Proin ullamcorper eu orci at volutpat. Donec quis vehicula nulla. Maecenas vel arcu sit amet purus tristique viverra ac a nunc. Curabitur varius enim ligula, at sagittis odio sollicitudin volutpat. Proin laoreet ac risus convallis consectetur. Donec non diam turpis. Aliquam nisl augue, elementum a accumsan ut, scelerisque vitae neque. Donec sed interdum nisl. Pellentesque rutrum eget sapien venenatis aliquet. Sed eros tortor, molestie et ornare quis, condimentum sit amet augue.",
                                    firstname: "Marc",
                                    id: 1430403539687,
                                    investment: 0,
                                    investmentNb: 0,
                                    surname: "Lenoir",
                                    title: "Nouveau SPAR Brest-Sud"
                                })
                            });
                        });
                    });
                    $localForage.setItem("initted", true);
                }
            })
        }]);