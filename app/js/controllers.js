angular.module('CasinoCrowdFunding.controllers', []).

    /* Home controller */
    controller('homeController', function ($scope, projects)
    {
        $scope.projects = projects;
        var project;
        $scope.options = {thickness: 5, mode: "gauge", total: 100};
        for (var i in projects)
        {
            project = projects[i];
            project.chartData = [
                {label: "", value: getPercent(project), color: "#157946", suffix: "%"}
            ];
        }
    }).

    /* Login controller */
    controller('loginController', function ($scope, $location, user, $routeParams)
    {
        $scope.user = user;

        $scope.submit = function ()
        {
            //console.log($scope.user);
            $scope.$root.setUser($scope.user)
            $location.path($routeParams.redirect);
        }
    }).

    /* Login controller */
    controller('logoutController', function ($scope, $location)
    {
        $scope.$root.logOut();
        $location.path('/')
    }).

    /* project form controller */
    controller('projectFormController', function ($scope, project, embeddedDbDAOService, $location, Notification)
    {
        $scope.project = project;

        $scope.submit = function ()
        {
            $scope.project.addressurl = "https://maps.googleapis.com/maps/api/staticmap?center=" + project.address.replace(/ /g, '+') + ",France&zoom=13&size=255x173&maptype=roadmap&markers=color:blue%7Clabel:S%7C40.702147,-74.015794&markers=color:green%7Clabel:G%7C40.711614,-74.012318&markers=color:red%7Clabel:C%7C40.718217,-73.998284";
            if ($scope.project.id)
            {

                embeddedDbDAOService.update("projects", $scope.project).then(function (data)
                {
                    $location.path('/projects/' + data.id);
                    Notification.success('Le projet a correctement été modifié.');
                })
            }
            else
            {
                embeddedDbDAOService.create("projects", $scope.project).then(function (data)
                {
                    $location.path('/projects/' + data.id);
                    Notification.success('Le projet a correctement été créé.');
                })
            }
        }
    }).

    /* project list controller */
    controller('projectsController', function ($scope, projects, embeddedDbDAOService)
    {
        $scope.projects = projects;

        var project;
        $scope.options = {thickness: 5, mode: "gauge", total: 100};
        for (var i in projects)
        {
            project = projects[i];
            project.chartData = [
                {label: "", value: getPercent(project), color: "#157946", suffix: "%"}
            ];
        }
    }).

    /* project controller */
    controller('projectController', function ($scope, project, embeddedDbDAOService, Notification)
    {
        $scope.project = project;

        $scope.investment = 5000;
        $scope.invest = function ()
        {
            embeddedDbDAOService.update("projects", $scope.project);
            project.investment += $scope.investment;
            project.investmentNb++;
            $scope.data[0].value = getPercent(project);
            Notification.success('Votre contribution a été prise en compte.');
        }
        $scope.data = [
            {label: "", value: getPercent(project), color: "#157946", suffix: "%"}
        ];
        $scope.options = {thickness: 10, mode: "gauge", total: 100};
    });

function getPercent(project)
{
    return Math.round(project.investment * 100 / project.amount);
}