'use strict';

/* Filters */

angular.module('CasinoCrowdFunding.filters', [])
    .filter('dateDiff', [function ()
    {
        return function (dtstr)
        {
            var timestamp = new Date(dtstr).getTime();
            var diff = timestamp - Date.now();
            var magicNumber = (1000 * 60 * 60 * 24);
            var days = Math.round(diff / magicNumber);
            return days;
        };
    }])
    .filter('reverse', [function ()
    {
        return function (items)
        {
            var output = [];
            for (var key in items)
            {
                output.unshift(items[key]);
            }
            return output;
        };
    }])
    .filter('thousands', [function ()
    {
        return function (number)
        {
            return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " ");
        };
    }])
    .filter('cut', [function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    }])
;