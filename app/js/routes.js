angular.module('CasinoCrowdFunding').
    config(['$routeProvider', function ($routeProvider)
    {
        $routeProvider.
            when("/", {
                templateUrl: "partials/home.html",
                controller: "homeController",
                resolve: {
                    projects: function (embeddedDbDAOService)
                    {
                        return embeddedDbDAOService.getAll("projects");
                    }
                }
            }).
            when("/login/:role", {
                templateUrl: "partials/login-form.html",
                controller: "loginController",
                resolve: {
                    user: function ($route)
                    {
                        return {
                            login: $route.current.params.role
                        };
                    }
                }
            }).
            when("/login", {
                templateUrl: "partials/login-form.html",
                controller: "loginController",
                resolve: {
                    user: function ()
                    {
                        return {};
                    }
                }
            }).
            when("/logout", {
                templateUrl: "partials/home.html",
                controller: "logoutController"
            }).
            when("/projects/new", {
                templateUrl: "partials/projects/project-form.html",
                controller: "projectFormController",
                resolve: {
                    project: function ()
                    {
                        var deadline = new Date();
                        deadline.setMonth(deadline.getMonth() + 8);
                        return {
                            amount: 100000,
                            age: 18,
                            investment: 0,
                            investmentNb: 0,
                            deadline: deadline.toJSON().slice(0, 10)
                        };
                    },
                    user: function ($rootScope, $location)
                    {
                        $rootScope.getUser().then(function (user)
                        {
                            if (!user)
                            {
                                var redirect = $location.path();
                                return $location.path('/login/projectMaker').search('redirect', redirect);
                            }
                            return user;
                        });
                    }
                }
            }).
            when("/projects/:id", {
                templateUrl: "partials/projects/project.html",
                controller: "projectController",
                resolve: {
                    project: function (embeddedDbDAOService, $route)
                    {
                        return embeddedDbDAOService.getOne("projects", $route.current.params.id);
                    },
                    user: function ($rootScope, $location)
                    {
                        $rootScope.getUser().then(function (user)
                        {
                            if (!user)
                            {
                                var redirect = $location.path();
                                return $location.path('/login/projectMaker').search('redirect', redirect);
                            }
                            return user;
                        });
                    }
                }
            }).
            when("/projects/:id/edit", {
                templateUrl: "partials/projects/project-form.html",
                controller: "projectFormController",
                resolve: {
                    project: function (embeddedDbDAOService, $route)
                    {
                        return embeddedDbDAOService.getOne("projects", $route.current.params.id);
                    },
                    user: function ($rootScope, $location)
                    {
                        $rootScope.getUser().then(function (user)
                        {
                            if (!user)
                            {
                                var redirect = $location.path();
                                return $location.path('/login/projectMaker').search('redirect', redirect);
                            }
                            return user;
                        });
                    }
                }
            }).
            when("/projects", {
                templateUrl: "partials/projects/project-list.html",
                controller: "projectsController",
                resolve: {
                    projects: function (embeddedDbDAOService)
                    {
                        return embeddedDbDAOService.getAll("projects");
                    },
                    user: function ($rootScope, $location)
                    {
                        $rootScope.getUser().then(function (user)
                        {
                            if (!user)
                            {
                                var redirect = $location.path();
                                return $location.path('/login/projectInvestor').search('redirect', redirect);
                            }
                            return user;
                        });
                    }
                }
            }).
            otherwise({
                redirectTo: '/'
            });
    }]);