angular.module('CasinoCrowdFunding.services', [])
    .factory('embeddedDbDAOService', function ($localForage)
    {
        //exposed api
        return {
            getAll: getTable,
            getOne: get,
            create: create,
            update: update,
            delete: remove
        }

        function get(entity, key)
        {
            return getTable(entity)
                .then(function (table)
                {
                    return table[key];
                }
            );
        }

        function create(entity, obj)
        {
            return getTable(entity)
                .then(function (table)
                {
                    var timestamp = +new Date;
                    obj.id = timestamp;
                    table[timestamp] = obj;
                    return $localForage.setItem(entity, table).then(function (data)
                    {
                        return obj
                    });
                }
            );
        }

        function update(entity, obj)
        {
            return getTable(entity)
                .then(function (table)
                {
                    table[obj.id] = obj;
                    return $localForage.setItem(entity, table).then(function (data)
                    {
                        return obj
                    });
                }
            );
        }

        function remove(entity, key)
        {
            alert("TODO : embeddedDbDAOService.remove()");
        }

        function getTable(tableName)
        {
            return $localForage.getItem(tableName)
                .then(function (table)
                {
                    if (!table)
                    {
                        return $localForage.setItem(tableName, {});
                    } else
                    {
                        return table;
                    }
                });
        }
    })
;